<?php include('header.php');?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.html">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Blank Page</li>
      </ol>
      <div class="row">
        <div class="col-12">
          <h1>Welcome <?php echo $_SESSION['user'];?></h1>
          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime culpa unde nemo enim eos expedita, vitae qui maiores laboriosam. Non tempore laudantium eius voluptatem quibusdam. Cumque vitae inventore odit beatae?</p>
        </div>
      </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->

<?php include('footer.php');?>
</body>

</html>
