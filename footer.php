<!-- Footer -->
<section id="footer">
		<div class="container">
						<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 mt-2 mt-sm-5">
					<ul class="list-unstyled list-inline social text-center">

						<?php 
						$st = "SELECT * FROM `tbl_social` where ID=1";
						$cm = $conn->prepare($st);
						$cm->execute();
						
						while($row = $cm->fetch(PDO::FETCH_ASSOC)){
						?>
						
						<?php 
						if(isset($row['facebook'])){
							?>
							<li class="list-inline-item"><a href="<?php echo $row['facebook'];?>"><i class="fa fa-facebook"></i></a></li>
							<?php
						}
						?>
						<?php 
						if(isset($row['twitter'])){
							?>
							<li class="list-inline-item"><a href="<?php echo $row['twitter'];?>"><i class="fa fa-twitter"></i></a></li>
							<?php
						}
						?>
						<?php 
						if(isset($row['instagram'])){
							?>
							<li class="list-inline-item"><a href="<?php echo $row['instagram'];?>"><i class="fa fa-instagram"></i></a></li>
							<?php
						}
						?>

						<?php 
						if(isset($row['googleplus'])){
							?>
							<li class="list-inline-item"><a href="<?php echo $row['googleplus'];?>"><i class="fa fa-google-plus"></i></a></li>
							<?php
						}
						?>
						
						
						<?php
						}
						?>
						
						
						
						
						
					</ul>
				</div>
				</hr>
			</div>	
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 mt-2 mt-sm-2 text-center text-white">
					
					<p class="h6">&copy All right Reversed.<a class="text-green ml-2" href="#" target="_blank">Project</a></p>
				</div>
				</hr>
			</div>	
		</div>
	</section>
	<!-- ./Footer -->
<script src="js/js.js"></script>
<script src="js/flick.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/app.js"></script>
<script type="text/javascript" src="js/jquery.fullPage.js"></script>
<script type="text/javascript">
		$(document).ready(function() {
			$('#fullpage').fullpage({
				sectionsColor: ['#1bbc9b', '#4BBFC3', '#7BAABE', 'whitesmoke', '#ccddff'],
				anchors: ['s1', 's2', 's3', 's4', 's5'],
				menu: '#menu',
				navigation: true,
				loopTop: true,
				loopBottom: true,
				
				//equivalent to jQuery `easeOutBack` extracted from http://matthewlein.com/ceaser/
				easingcss3: 'cubic-bezier(0.175, 0.885, 0.320, 1.275)'
			});

		});
	</script>

</body>
</html>