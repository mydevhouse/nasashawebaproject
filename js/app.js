
$(document).ready(function(){
//your js here

 $('#characterLeft').text('140 characters left');
    $('#message').keydown(function () {
        var max = 140;
        var len = $(this).val().length;
        if (len >= max) {
            $('#characterLeft').text('You have reached the limit');
            $('#characterLeft').addClass('red');
            $('#btnSubmit').addClass('disabled');            
        } 
        else {
            var ch = max - len;
            $('#characterLeft').text(ch + ' characters left');
            $('#btnSubmit').removeClass('disabled');
            $('#characterLeft').removeClass('red');            
        }
    });
    var prettyprint = false;
    $("pre code").parent().each(function()
    {
        $(this).addClass('prettyprint linenums');
        prettyprint = true;
    });	
    if ( prettyprint )
    {
        $.getScript("https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js", function() { prettyPrint() });
    }
//end
});



